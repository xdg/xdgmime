/*
 * Copyright 2023 GNOME Foundation Inc.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later or AFL-2.0
 */

#include <stddef.h>

void fuzz_teardown (int working_dir_fd);
int fuzz_setup (const unsigned char *data,
                size_t               data_len,
                int                 *working_dir_fd_out);

int LLVMFuzzerTestOneInput (const unsigned char *data,
                            size_t               size);
