/*
 * Copyright 2023 GNOME Foundation Inc.
 *
 * SPDX-License-Identifier: LGPL-2.1-or-later or AFL-2.0
 */

#include <stddef.h>

#include "setup.h"
#include "xdgmime.h"

int
LLVMFuzzerTestOneInput (const unsigned char *data,
                        size_t               size)
{
  int working_dir_fd;

  /* Silently return success on setup failure. Any non-success exit status
   * counts as a fuzzing failure and indicates a bug. */
  if (!fuzz_setup (data, size, &working_dir_fd))
    return 0;

  xdg_mime_dump ();

  fuzz_teardown (working_dir_fd);

  return 0;
}
